
//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 3
//Date:     11/26/19
//-----------------------------------------------------------------------------

/*
 * NOTE: ALL NEW FUNCTION IN THIS FILE ARE FROM testScanner.cpp in P1.
 * As this is no longer about testing scanner they have been moved here.
 * testScanner can be found on https://gitlab.com/JonathanGabler/4280-p1/
 * */

#include "scanner.h"
#include "token.h"
#include <string>
#include <cctype> //For isdigit, isspace, islower, isalpha functions
#include <cstddef>
#include <iostream>
#include <stdlib.h> //For the exit() function

//extern from token.h
int current_token_index;

//extern from token.h
int current_line_index;

//extern from token.h
int current_index;

//comment flag
static int comment_flag;

vector<string> file_string;

/// Test the passed char to match it up with the correct column in state table
/// \param character
/// \return
int getColumn(char character){
    //Is char a letter?
    if(isalpha(character)){
        //Is char a lowercase?
        if(islower(character))
            return 0;
        else
            return 1;
    }
    else if(isdigit(character))
        return 2;
    else if(isOp(character))
        return 3;
    else if(isspace(character))
        return 4;
    else {
        cout << "When matching char to column no match was found. Character tested was: " << character << "\n";
        return -1;
    }
}

/// throw error to user to let them know scanner failed at what instance
/// \param state
/// \param input_string
void errorOccured(int state, string input_string){
    //Print out fatal error when scanning
    cout << "ERROR: invalid character on line: " << current_line_index << " at character: " << current_index;
    cout << "PASSED STRING: " + input_string;

    if(state == ERROR_DIGIT){
        cout << "ERROR: digit tokens must contain only digits\n";
    }
    else if(state == ERROR_UPPERCASE){
        cout << " ERROR Meaning: Tokens must start with lowercase letter followed by 3 or more upper case, lower case or digit characters\n";
    }
    else
        cout << " ERROR Meaning: No idea what went wrong here to be honest...";

}

/// Test the character is valid or not using given grammar in token.h
/// \param character
/// \return boolean
bool isValidCharacter(char character){
    return !(!isOp(character) && !isdigit(character) && !isalpha(character));
}

/// This is testScanner() function in p1 testScanner.cpp
/// \param in
void readFile(istream &in){
    //Build maps
    buildOpMap();
    buildkeywordMap();

    string string_input_line;
    Token token;

    //set the current line index to first line in file
    current_line_index = 0;
    int counter = 0;

    //Loop through file
    while(getline(in, string_input_line)){
        current_index = 0;

        //pass string to filter to remove unwanted characters
        filter(string_input_line);

        //Pass filtered string to testing file vector
        if(string_input_line.length() > 0){
            file_string.push_back(string_input_line);
            counter++;
        }
    }

}

/// Filter the passed in string
/// \param unfiltered_string
/// \return
int filter(string &unfiltered_string){
/*    //For debugging string passed
    cout << "Unfiltered string: " << endl << unfiltered_string << endl << endl;*/

    //String to check overflow of current index line index compared to passed string
    if(current_index >= unfiltered_string.length())
        return 0;

    string filtered_string;
    char past_character;
    char current_character;
    char SPACE = ' ';

    //loop through the unfiltered string
    for(int i = current_index; i < unfiltered_string.length(); i++){
        current_character = unfiltered_string.at(i);

        if(i > 0){
            //set past char to previous one in string
            past_character = unfiltered_string.at(i-1);
        }

        //check for damn comments. NOT SURE IF THIS PART WORKS. only did sometimes...
        if(current_character == COMMENT){
            filtered_string.push_back(SPACE);

            //If the comment flag is set then turn off because marks end of comment
            //If the comment flag is not set then turn on becuase marks start of comment
            comment_flag = !comment_flag;
        }
            //character is not a comment
        else if(!comment_flag){
            //check if is a space or not
            if(isspace(current_character)){
                if(!isspace(past_character)){
                    filtered_string.push_back(current_character);
                }
            }
                //Not a comment or a space so test if its in our grammar rules
            else if(!isValidCharacter(current_character)){
                //Character is not grammar throw error
                cout << "ERROR: invalid character on line: " << current_line_index << " at character: " << current_index;
                return -1;
            }
                //Not a coment or a space but is a valid character in the grammar
            else{
                filtered_string.push_back(current_character);
            }
        }

        current_index++;
    }

    //Find last index not a ws. spaces are treated as comments and must be stripped
    string whitespaceDelimenters = " \t\f\v\n\r";

    size_t trailing_ws_index = filtered_string.find_last_not_of(whitespaceDelimenters);

    //Remove any trailing ws
    if(trailing_ws_index != string::npos){
        filtered_string.erase(trailing_ws_index + 1);
    }
    //If none found continue
    else{
        //Check to make sure the whole line is not a comment
        bool wholeLineComment_flag = true;

        //loop through line
        for(int i = 0; i < filtered_string.length(); i++){
            if(filtered_string.at(i) == SPACE){
                wholeLineComment_flag = false;
                break;
            }
        }
        if(wholeLineComment_flag){
            filtered_string.assign("");
        }

    }
    return current_index;

}

/// Scanner function
/// \param token
/// \return
int scanner(Token &token){

    string input_file_string = getString();

    //EOF check
    if(current_token_index == input_file_string.length()){
        current_line_index++;
        current_token_index = 0;

        if(current_line_index < file_string.size()){
            input_file_string = file_string[current_line_index];
        }
        //set EOF token
        else{
            token.tk_Description = "EOF";
            token.ID = EOFtk;
            token.line_Number = current_line_index-1;
            return 1;
        }
    }

    //line number
    token.line_Number = current_line_index + 1;

    int current_state = 0;
    // FSA Rows
    int next_state;
    // FSA columns
    int next_input;

    char next_character;
    const char SPACE = ' ';
    string token_description;

    //loop through string to classify tokens one by one.
    while(current_token_index <= input_file_string.length()){
        if(current_token_index < input_file_string.length()){
            next_character = input_file_string.at(current_token_index);
        }
            //END OF STRING REACHED BUT STILL NEEDS COUNTED.
        else
            next_character = SPACE;

        //GET COLUMN
        next_input = getColumn(next_character);
        // Get the row
        next_state = FSA_TABLE[current_state][next_input];

        //FSA returned a value graeter than 1000
        if(next_state > 1000){

            token.tk_Description = token_description;
            //switch to find which final case occured
            switch (next_state){
                case ID_FINAL_STATE:
                    if(isKeyword(token) != -1){
                        token.ID = KEYWORDtk;
                        token.tk_Description.assign("" + token_description);
                    }
                    else{
                        token.ID = IDtk;
                        token.tk_Description.assign(token_description);
                    }
                    break;
                case INT_FINAL_SATE:
                    token.ID = INTtk;
                    token.tk_Description.assign(token_description);
                    break;
                case OP_FINAL_STATE:
                    token.ID = OPtk;
                    getOp(token);
                    token.tk_Description.assign(token_description);
                    break;
            }

            return 0;
        }
            // If FSA table returns a negative number
        else if(next_state < 0){
            //reset
            current_state = 0;
            //call errorOccured to display error
            errorOccured(next_state, input_file_string);
            //Move to next token
            current_token_index++;
            //Abandon ship
            exit(EXIT_FAILURE);
        }

        current_state = next_state;
        current_token_index++;

        if(!isspace(next_character))
            token_description.push_back(next_character);
    }
    //If you reach this point some serouis shit went wrong
    return -1;
}

/// Get the current line from the file.
/// \return current file line
string getString()
{
    return file_string[current_line_index];
}
