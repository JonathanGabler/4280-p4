//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 3
//Date:     11/26/19
//-----------------------------------------------------------------------------

#ifndef INC_4280_P2_TESTTREE_H
#define INC_4280_P2_TESTTREE_H

#include "node.h"

using namespace std;
//Test tree prototype

void preorder(node_t*, int);

#endif //INC_4280_P2_TESTTREE_H
