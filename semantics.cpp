//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 3
//Date:     11/26/19
//-----------------------------------------------------------------------------

#include <iostream>
#include <stdlib.h>
#include <vector>
#include "semantics.h"
#include "token.h"
#include "node.h"

//Temp stack
static stack_t temp;
//variable scope
static int varScope = 0;
//stack current index
int stackIndex = 0;
//number of indexs on stack
int stackIndexCount = 0;
//repeat variables
static int repeatNumber = 0;
//temp variables in scope
static int tempVar = 0;

//vector for variables
vector<stack_t> stack;

/*
 * Print the stack
 *
 * */
void printStack(){
    //iterate through stack
    for(int i =0; i<stack.size(); i++){
        cout << "\tIndex " << i << " = " << stack.at(i).token.tk_Description
        << " Current Scope: " << stack.at(i).scope << "\n";
    }
}


/*
 * Check var is used to check if a variable is already defined or not in the current scope.
 * If not defined push to stack and continue. Otherwise error out
 *
 * */
void checkVar(stack_t variable){
    //check if already defined
    int varDefined = findVar(variable);

    //If already defined error out
    if(varDefined > 0){
        cout << "SEMANTICS ERROR: " << varDefined << " on line: " << variable.token.line_Number
        << "has already been defined in the current scope at: " << stack.at(varDefined).token.line_Number << "\n";

        //exit
        exit(EXIT_FAILURE);
    }
    //else it was not already defijned push to stack
    else{
        //push the var to the stack
        stack.push_back(variable);
        //increase the stack
        stackIndex++;
        //printStack();
    }
}

/*
 * checkVar is used to check if var is defined before it is used
 *
 * */
int checkVarExist(stack_t variable){
    //check stack for var
    for(int i = 0; i<stack.size(); i++){
        if(stack.at(i).token.tk_Description == variable.token.tk_Description){
            return i;
        }
    }

    //else return
    return -1;
}

/*
 * If the program defines a new variable check to make sure if it has been defined in the
 * given scope already
 *
 */

int findVar(stack_t variable){

    //loop through stack
    for(int i = 0; i<stack.size(); i++){
        if((stack.at(i).token.tk_Description == variable.token.tk_Description)
        && (stack.at(i).scope == variable.scope)){
            return i;
        }
    }

    return -1;
}

int findStackLocation(stack_t var){
    for(int i=0; i<stack.size(); i++){
        if((stack.at(i).token.tk_Description == var.token.tk_Description) && (stack.at(i).scope <= var.scope)){
            return i;
        }
    }

    cout<< "ID: " << var.token.tk_Description << " was not declared in current scope.\n";
    exit(EXIT_FAILURE);
}

/*
 * CompareScope is used to compare the scope one variable is being used in to another to
 * make sure they are not being used in the same scope
 *
 * */

int compareScope(stack_t variable){
    //check if variable has been defined
    int varLocation = checkVarExist(variable);

    if(varLocation >= 0){
        //Var has been defined
        if(stack.at(varLocation).scope >variable.scope){
            //The var was declared in a later scope and connot be used in this one
            cout << "SEMANTICS ERROR: " << getTokenDesc(variable.token) << " on line: " << variable.token.line_Number
            << " cannot be accessed in current scope\n";
            exit(EXIT_FAILURE);
        }
        else
            return 1;
    }
    else{
        //variable is not found in stack
        cout << "SEMANTICS ERROR: " << getTokenDesc(variable.token) << " on line: " << variable.token.line_Number
             << " is not found or cannot be accessed in current scope\n";
        exit(EXIT_FAILURE);
    }
}

/*
 * removeLocalVar(int) is used to clear out the scope once you exit it.
 *
 * */

void removeLocalVar(int scope){
    //make sure you aren't in main scope
    if(scope>0){
        //clear the stack
        for(int i = 0; i<stack.size(); i++){
            if(stack.at(i).scope == scope){
                //pop pushed to asm file
                targetFile << "POP\n";
                stack.erase(stack.begin() + i);
            }
        }
    }
    else if(scope == 0){
        int stackSize = stack.size();
        stackSize--;
        for(stackSize; stackSize > 0; stackSize--){
            if(stack.at(stackSize).scope == scope){
                targetFile << "POP\n";
                stack.erase(stack.begin() + stackSize);
            }
        }

    }
}

/*
 * search through the stack for the following rules.
 *
 * Variables have to be defined before used first time (must satisfy syntax too)
 * Variable name can only be defined once
 * NOTE: EACH NEW NESTED BLOCK CAN USE A PREVIOUSILY NAMED VARIABLE AS A NEW VARIABLE.
 *      Thus each new block will be a new level on the local stack to seperate them.
 *
 * */

void semantics(node_t *root) {

    //check if the tree is null
    if(root == NULL){
        cout<< "The node tree is NULL. Ending Semantics process";
        return;
    }

    //<S>->program <V> <B>
    if(root->label == "<S>"){
       //DEBUG cout
/*
        cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        //process the children
        semantics(root->child1);
        semantics(root->child2);



        //clear stack
        removeLocalVar(varScope);
        varScope--;
        //Program terminating
        targetFile << "STOP\n";


        while(tempVar > 0){
            targetFile << "T" << (tempVar -1) << " 0\n";
            cout <<  "T" << (tempVar -1) << " 0\n";
            tempVar --;
        }

        //Terminate semantics if no errors
        cout<< "************** SEMANTICS CHECK IN PROGRESS ***********\n";
        cout << "SEMANTICS CHECK: PASSED\n";
        return;
    }

    //<B> -> begin <V> <Q> end
    if(root->label == "<B>"){
        //increase the scope since new nested block
        varScope++;

        //DEBUG cout
/*
        cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        //Process the children
        semantics(root->child1);
        semantics(root->child2);

        //children process done. decrement scope and clear local cache variables
        removeLocalVar(varScope);
        varScope--;

    }

    //<V> -> empty | var identifier . <V>
    if(root->label == "<V>"){

        //DEBUG cout
/*
        cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        //create temp token at front of current token
        temp.token = root->tokens.front();
        temp.scope = varScope;
        root->tokens.erase(root->tokens.begin());

        //check if temp token is empty
        if(temp.token.tk_Description != "EMPTY"){
            //token is not empty thus var id . <v> case
            //check if var was declared alread
            checkVar(temp);
            //add var to stack
            stackIndexCount++;
            //process var
            semantics(root->child1);
        }
    }

    //<M> -> <H> + <M> | <H> - <M> | <H> / <M> | <H> * <M> | <H>
    if(root->label == "<M>"){

        //DEBUG cout
/*
        cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        // <M> -> <H> case
        if(root->tokens.empty()){
            semantics(root->child1);
        }
            // all other cases for <M>
        else{

            int orginalVar = (tempVar)-1 ;
            int localVar = tempVar;
            //debug temp cound
            //targetFile << "current temp #  = " <<tempVar <<"\n";
            tempVar++;

            //<M>
            semantics(root->child2);

            //write localvar to acc
            targetFile << "STORE T" << localVar << "\n";
            cout << "STORE T" << localVar << "\n";

            targetFile << "LOAD T" << orginalVar << "\n";
            cout << "LOAD T" << orginalVar << "\n";


            // <H>
            semantics(root->child1);

            //check for what op was used
            if(op_map[root->tokens.at(0).tk_Description] == "PLUStk"){
                targetFile << "ADD T" << localVar << "\n";
                cout << "ADD T" << localVar << "\n";
            }
            else if(op_map[root->tokens.at(0).tk_Description] == "MINUStk"){
                targetFile << "SUB T" << localVar << "\n";
                cout << "SUB T" << localVar << "\n";
            }
            else if(op_map[root->tokens.at(0).tk_Description] == "SLASHtk"){
                targetFile << "DIV T" << localVar << "\n";
                cout << "DIV T" << localVar << "\n";
            }
            else if(op_map[root->tokens.at(0).tk_Description] == "ASTERIKtk"){
                targetFile << "MULT T" << localVar << "\n";
                cout << "MULT T" << localVar << "\n";
            }

            return;
        }
    }

    //<H> -> & <R> | <R>
    if(root->label == "<H>"){

        //DEBUG cout
/*
        cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        //process child
        semantics(root->child1);
    }

    //<R> -> identifier | number
    if(root->label == "<R>"){

       //DEBUG cout
/*
        cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        //if id token make sure var was not previsoulsy declared
        //setup temp token to current token
        temp.token = root->tokens.front();
        temp.scope = varScope;

        if(temp.token.ID == IDtk){
            compareScope(temp);
        }
        else if(temp.token.ID == INTtk){
            //store number
            targetFile << "LOAD " << getTokenInt(temp.token) << "\n";
            cout << "LOAD " << getTokenInt(temp.token) << "\n";
        }
        return;

    }

    //<Q> -> <T> # <Q> | empty
    if(root->label == "<Q>"){

        //DEBUG cout
/*
        cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        //create temp token at front of current token
        temp.token = root->tokens.front();
        temp.scope = varScope;
        root->tokens.erase(root->tokens.begin());

        //check if temp token is empty
        if(temp.token.tk_Description != "EMPTY"){

            //process var
            semantics(root->child1); //<T>
            semantics(root->child2); //<Q>
        }
    }

    //<T> -> <A> , | <W> , | <B> | <I> , | <G> , | <E> ,
    if(root->label == "<T>"){

        //DEBUG cout
/*
        cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        //process the only child
        semantics(root->child1); // <A> , | <W> , | <B> | <I> , | <G> , | <E> ,
    }

    //<A> -> scan identifier | scan number
    if(root->label == "<A>"){

        //DEBUG cout
/*
        cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        //if id token make sure var was not previsoulsy declared
        //setup temp token to current token
        temp.token = root->tokens.front();
        temp.scope = varScope;

        int localVar = tempVar;
        tempVar++;


        if(temp.token.ID == IDtk){
            //scan the id using temp var
            compareScope(temp);
            targetFile << "READ T" <<localVar << "\n";
            cout << "READ T" <<localVar << "\n";

        }
        else if(temp.token.ID == INTtk){
            //scan the number using temp var and set value to number
            targetFile << "READ T" << localVar << "\n";
            cout << "READ T" << localVar << "\n";
        }
        return;
    }

    //<W> -> write <M>
    if(root->label =="<W>"){

        //DEBUG cout
/*
        cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        //setup temp
        int localVar = (tempVar)-1;

        //process child
        semantics(root->child1); //<M>
        //write to asm file
        targetFile << "WRITE T" << localVar << "\n";
        cout << "WRITE T" << localVar << "\n";
    }

    //<I> -> if [ <M> <Z> <M> ] <T>
    if(root->label == "<I>"){

        //DEBUG cout
/*
       cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        int loop = ++repeatNumber;
        int localVar = tempVar;
        tempVar ++;

        //process first comparision
        semantics(root->child1); //<M>
        //set local var to value in acc
        targetFile << "STORE T" << localVar << "\n";
        cout << "STORE T" << localVar << "\n";

        //process second comparrison
        semantics(root->child3); //<M>
        //subtract value in acc from temp
        targetFile << "SUB T" << localVar << "\n";
        cout << "SUB T" << localVar << "\n";

        //run other children
        semantics(root->child2); //<Z>
        semantics(root->child4); //<T>

        //declare label
        targetFile << "label" << loop << ": NOOP\n";
        cout << "label" << loop << ": NOOP\n";

        return;
    }

    //<G> -> repeat [ <M> <Z> <M> ] <T>
    if(root->label == "<G>"){

        //DEBUG cout
/*
        cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        int loop = ++repeatNumber;
        int localVar = tempVar;
        tempVar++;

        //run first comparision
        semantics(root->child1); //<M>
        //assign temp to value in acc
        targetFile << "STORE T" << localVar << "\n";
        cout << "STORE T" << localVar << "\n";


        //run second comparison
        semantics(root->child3); //<M>
        //subtract value in acc from temp
        targetFile << "SUB T" << localVar << "\n";
        cout << "SUB T" << localVar << "\n";


        //run remaining children
        semantics(root->child2); //<Z>
        semantics(root->child4); //<T>

        //jump to loop
        targetFile << "BR loop" << loop << "\n";
        cout << "BR loop" << loop << "\n";


        targetFile << "label" << loop << ": NOOP\n";
        cout << "label" << loop << ": NOOP\n";
    }

    //<E> -> let identifier : <M>
    if(root->label == "<E>"){

        //DEBUG cout
/*
        cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        //check if id was declared already
        temp.token = root->tokens.front();
        temp.scope = varScope;

        compareScope(temp);
        stackIndexCount ++;

        //process the child
        semantics(root->child1);

        return;
    }

    //<Z> -> < | > | : | = | = =
    if(root->label == "<Z>"){

        //DEBUG cout
/*
        cout<<"***DEBUG: Current root: " << root->label << " Current scope: " << varScope << "***\n";
*/

        //create new temp stack
        temp.token = root-> tokens.front();
        temp.scope = varScope;
        root->tokens.erase(root->tokens.begin());

        //test for <
        if(op_map[temp.token.tk_Description] == "LESSTHANtk"){
            //check for <= rule
            if(!root->tokens.empty()){
                //cerate a second temp stack
                stack_t temp2;
                temp2.token = root->tokens.front();
                temp2.scope = varScope;
                root->tokens.erase(root->tokens.begin());

                //test if second op is =
                if(op_map[temp2.token.tk_Description] == "EQUALStk"){
                    //BRNEG case
                    targetFile << "BRNEG label" << repeatNumber << "\n";
                    cout << "BRNEG label" << repeatNumber << "\n";
                    return;
                }

            }
            //root is empty so just < case
            targetFile << "BRZNEG label" << repeatNumber << "\n";
            cout << "BRZNEG label" << repeatNumber << "\n";
            return;
        }

        //test for >
        else if(op_map[temp.token.tk_Description] == "GREATERTHANtk"){
            //check for >= rule
            if(!root->tokens.empty()){
                //cerate a second temp stack
                stack_t temp2;
                temp2.token = root->tokens.front();
                temp2.scope = varScope;
                root->tokens.erase(root->tokens.begin());

                //test if second op is =
                if(op_map[temp2.token.tk_Description] == "EQUALStk"){
                    //BRNEG case
                    targetFile << "BRPOS label" << repeatNumber << "\n";
                    cout << "BRPOS label" << repeatNumber << "\n";

                    return;
                }

            }
            //root is empty so just > case
            targetFile << "BRZPOS label" << repeatNumber << "\n";
            cout << "BRZPOS label" << repeatNumber << "\n";

            return;
        }

        //test for =
        else if(op_map[temp.token.tk_Description] == "EQUALStk"){
            //Check for == rule
            if(!root->tokens.empty()){
                //cerate a second temp stack
                stack_t temp2;
                temp2.token = root->tokens.front();
                temp2.scope = varScope;
                root->tokens.erase(root->tokens.begin());

                //test if second op is =
                if(op_map[temp2.token.tk_Description] == "EQUALStk"){
                    //== is treated as = in our semantics
                    targetFile << "BRPOS label" << repeatNumber << "\n";
                    cout << "BRPOS label" << repeatNumber << "\n";

                    targetFile << "BRNEG label" << repeatNumber << "\n";
                    cout << "BRNEG label" << repeatNumber << "\n";

                    return;
                }

            }
            //root is empty so just = case
            targetFile << "BRPOS label" << repeatNumber << "\n";
            cout << "BRPOS label" << repeatNumber << "\n";

            targetFile << "BRNEG label" << repeatNumber << "\n";
            cout << "BRNEG label" << repeatNumber << "\n";

            return;
        }

    }

}
