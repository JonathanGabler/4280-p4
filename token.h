//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 3
//Date:     11/26/19
//-----------------------------------------------------------------------------

#ifndef INC_4280_P2_TOKEN_H
#define INC_4280_P2_TOKEN_H

#include <map>
#include <string>
using namespace std;

//comments constant
const char COMMENT = '%';

//Token count
const int TOKEN_COUNT = 5;
//Add the name of each token to array
const string TOKEN_NAMES[TOKEN_COUNT] = {"Identifier", "keyword", "Operator", "Integer", "End of File"};
//Token categories defined in state table
enum token_ID {IDtk, KEYWORDtk, OPtk, INTtk, EOFtk};

// Keyword count
const int KEYWORD_COUNT = 12;
//Array of keywords allowed
const string KEYWORDS[KEYWORD_COUNT] = {"begin", "end", "repeat", "if", "then", "let", "void", "var", "return", "scan", "program", "write"};
//Map keywords to tokens
extern map<string, string> keyword_map;

//Operator count
const int OPERATOR_COUNT = 21;
//Array of operators allowed
const char OPERATORS[] = {'#', '=', '<', '>', ':', '+','-', '*', '/', '%', '.', '(', ')', ',', '{', '}', ';', '[', ']','&'};
//Map operators to tokens
extern map<string, string> op_map;

//Current index within the current line
extern int current_index;

//current char within the current token
extern int current_token_index;

// Current line index
extern int current_line_index;

//Token struct
struct Token{
    token_ID ID;
    string tk_Description;
    int line_Number;
};

//Function prototypes
void buildkeywordMap();
void buildOpMap();
void getOp(Token &);
int isOp(char);
int isKeyword(Token &);
void tokenDisplay(Token);
string getTokenDesc(Token);
string getTokenInt(Token);




#endif //INC_4280_P2_TOKEN_H
