//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 3
//Date:     11/26/19
//-----------------------------------------------------------------------------

#ifndef FRONTEND_NODE_H
#define FRONTEND_NODE_H

#include "token.h"
#include <string>
#include <vector>
#include <set>

//getting sick of having to constantly add this...
using namespace std;

struct node_t{
    int tokenSize;
    int level;
    string label;
//token vector
    vector<Token> tokens;

//Max children for language is 4
//NOTE THIS DOES NOT INCLUDE KEYWORDS
//IF WE INCLUDE KEYWORDS THEN MAX CHILDREN WOULD BE 7
    struct node_t *child1, *child2, *child3, *child4;
};


#endif //FRONTEND_NODE_H
