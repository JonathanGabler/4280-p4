//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 3
//Date:     11/26/19
//-----------------------------------------------------------------------------

#ifndef STATSEM_SEMANTICS_H
#define STATSEM_SEMANTICS_H

#include <vector>
#include <iostream>
#include <fstream>
#include "token.h"
#include "node.h"

using namespace std;

//variable stuct
struct stack_t{
    Token token;
    int lineNumber;
    int scope;
};

//variable stack vector
extern vector<stack_t> stack;
extern ofstream targetFile;

//Prototypes
int checkVarExist(stack_t);
int findVar(stack_t);
int findStackLocation(stack_t);
int compareScope(stack_t);
void checkVar(stack_t);
void removeLocalVar(int);
void printStack();
void semantics(node_t*);

//constant
const int MAX_VARS = 100;

#endif //STATSEM_SEMANTICS_H
