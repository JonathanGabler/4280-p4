//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 3
//Date:     11/26/19
//-----------------------------------------------------------------------------

#include "token.h"
#include <string>
#include <iostream>
#include <map>

//Display the token
void tokenDisplay(Token token){
    cout << "Line #: " << token.line_Number << " TokenID: " << token.ID <<" Description: " << token.tk_Description + "\n";
}
//Keyword to Token map
map<string, string> keyword_map;

//Build the map
void buildkeywordMap(){
    keyword_map.insert(make_pair("begin", "BEGINtk"));
    keyword_map.insert(make_pair("if", "IFtk"));
    keyword_map.insert(make_pair("end", "ENDtk"));
    keyword_map.insert(make_pair("void", "VOIDtk"));
    keyword_map.insert(make_pair("var", "VARtk"));
    keyword_map.insert(make_pair("return", "RETURNtk"));
    keyword_map.insert(make_pair("read", "READtk"));
    keyword_map.insert(make_pair("write", "WRITEtk"));
    keyword_map.insert(make_pair("program", "PROGRAMtk"));
    keyword_map.insert(make_pair("scan", "SCANtk"));
    keyword_map.insert(make_pair("then", "THENtk"));
    keyword_map.insert(make_pair("let", "LETtk"));
    keyword_map.insert(make_pair("repeat", "REPEATtk"));
}

//Operator to token map
map<string, string> op_map;

//Build the map
void buildOpMap(){
    op_map.insert(make_pair("=", "EQUALStk"));
    op_map.insert(make_pair("&", "ANDtk"));
    op_map.insert(make_pair("<", "LESSTHANtk"));
    op_map.insert(make_pair(":", "COLONtk"));
    op_map.insert(make_pair(">", "GREATERTHANtk"));
    op_map.insert(make_pair("+", "PLUStk"));
    op_map.insert(make_pair("-", "MINUStk"));
    op_map.insert(make_pair("*", "ASTERIKtk"));
    op_map.insert(make_pair("/", "SLASHtk"));
    op_map.insert(make_pair("#", "HASHtk"));
    op_map.insert(make_pair("%", "MODULUStk"));
    op_map.insert(make_pair(".", "PERIODtk"));
    op_map.insert(make_pair("(", "LEFTPARENtk"));
    op_map.insert(make_pair(")", "RIGHTPARENtk"));
    op_map.insert(make_pair(",", "COMMAtk"));
    op_map.insert(make_pair("{", "LEFTBRACEtk"));
    op_map.insert(make_pair("}", "RIGHTBRACEtk"));
    op_map.insert(make_pair(";", "SEMICOLONtk"));
    op_map.insert(make_pair("[", "LEFTBRACKETtk"));
    op_map.insert(make_pair("]", "RIGHTBRACKETtk"));
}

//Check if the current token is keyword
int isKeyword(Token &token){
    //Check if the current string is in the allowed keywords array
    for(int i = 0; i < KEYWORD_COUNT; i++){
        if(token.tk_Description == KEYWORDS[i]){
            token.tk_Description = keyword_map[token.tk_Description];
            return i;
        }
    }
    //If not found
    return -1;
}

//Check if current char in test is a operator
int isOp(char character){
    //check char with alloweed ops in op array
    for(int i = 0; i < OPERATOR_COUNT; i++){
        if(character == OPERATORS[i]){
            // The character is in allowed op list
            return 1;
        }
    }

    //If not found in loop then character is not in op list
    return 0;
}

// Assign the op to the token discription from map
void getOp(Token &token){
    token.tk_Description.assign(op_map[token.tk_Description]);
}

//get token description
string getTokenDesc(Token token){
    size_t found = token.tk_Description.find("IDtk");
    string desc = token.tk_Description;
    if(found != string :: npos){
        desc.erase(found, 5);
    }
    return desc;
}

//Get token int
string getTokenInt(Token token){
    size_t found = token.tk_Description.find("INTtk");
    string description = token.tk_Description;
    if(found != string::npos){
        description.erase(found, 6);
    }
    return description;
}



