# Compiler P4

> This is a compiler developed for CS4280 Program Translation at UMSL. This program uses a State Table for parsing.

Read Project P4.pdf for project details.

## Notice
* ALL ERRORS FROM PAST PROJECTS CORRECTED FOR REGRADING.
* Program uses temp vars for each var. Found it much easier 
this way to avoid constant stack reads.

## Requirements

C/C++ compiler.

## Usage Example

The included makefile can be used as such:

```sh
~$ make
```
To run the project after you build it:
```sh
~$ ./compile filename
```
Where filename is the file you wish to test. Provided ones in Test-Files dir

## Grammar
Provided grammar for the project
```
<S> -> program <V> <B>
<B> -> begin <V> <Q> end
<V> -> empty | var identifier . <V>
<M> -> <H> + <M> | <H> - <M> | <H> / <M> | <H> * <M> | <H>
<H> -> & <R> | <R>
<R> -> identifier | number
<Q> -> <T> # <Q> | empty
<T> -> <A> , | <W> , | <B> | <I> , | <G> , | <E> ,
<A> -> scan identifier | scan number
<W> -> write <M>
<I> -> if [ <M> <Z> <M> ] <T>
<G> -> repeat [ <M> <Z> <M> ] <T>
<E> -> let identifier : <M>
<Z> -> < | > | : | = | = =

```
## Grammar Semantics
* Program executes sequentially from beginning to end, one statement at a time
* Delimiters:
    * ``` . , # [ ] ```
* Less than and greater than are standard symbols
    * ``` < > ```
* ‘&’ means absolute value for number
    * ``` Number may be immediate or stored at location indicated by identifier ```
* The following operators have standard definitions, except no precedence
    * ``` + - * / ```
    * ``` Operations are applied from left to right ```
* ‘let identifier : M’ assigns the value of M to the given identifier
* ‘=’ and ‘==’ are both treated like standard ‘==’
    * ``` Assignment of values to variables can only be performed using ‘[let] identifier : M’ ```
* ‘if [ M Z M ] T’
    * ``` Means to do T if and only if (M Z M) is true (always true if Z is ‘:’) ```
* ‘repeat [ M Z M ] T’
    * ``` Means to repeat T until (M Z M) is false ```
* When scanning an identifier, allocate memory using the identifier as the variable name and 
  set value to zero
* When scanning a number, allocate memory using a temporary variable name and set value to the
  number

## Target Language
```$xslt
• VM ACCumulator assembly language
• Executable is available on Canvas (virtMach)
• Description provided in VM_Architecture.pdf and VM_Language.pdf on Canvas
```
## Contents 

Here is a list of the included files and their usage in this project:

* ``` main.cpp ```
  * handles the input from either passed file or command line input, and calls parse class
* ``` node.h ```
  * data structure for use with parse tree
* ``` scanner.h & scanner.cpp ```
  * used by the parser to create tokens from the source code
* ``` parser.h & parser.cpp ```
  * checks the tokens in the parse tree to check if they conform to the requirements of the course code grammar
* ``` testTree.h & testTree.cpp ```
  * display the generated parse tree
* ``` token.h & token.cpp ```
  * Builds the structure for our tokens and creates maps for tokens
* ``` semantics.h & semantics.cpp ```
  * Processes the semantic rules for the grammar.
* ``` Test-Files Directory  ```
  * Sample test files to run.

## Sample Run
* INPUT
* ```
  program
  var id1 .
  begin
  let id1 : 5 ,
  #
  if [ id1 > 3 ]
  write id1 - 2 , ,
  #
  end
    ```
 * OUTPUT
 * ```
   LOAD 5
   STORE T0
   LOAD 3
   SUB T0
   BRZPOS label1
   LOAD 2
   STORE T1
   LOAD T0
   SUB T1
   WRITE T0
   label1: NOOP
   STOP
   T1 0
   T0 0

    ```
 
## About

* Author: Jonathan Gabler
* Contact: JonGabler@outlook.com jegq6b@mail.umsl.edu
* Organization: UMSL

## Known Issues
* None at the moment 





