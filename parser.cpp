//-----------------------------------------------------------------------------
//Author:   Jonathan Gabler
//Class:    CS4280 Program Translations
//Assign:   Project 3
//Date:     11/26/19
//-----------------------------------------------------------------------------


#include "parser.h"
#include "token.h"
#include "scanner.h"
#include "node.h"
#include <iostream>
#include <stdlib.h>

using namespace std;

Token token;
Token EMPTY_TOKEN;
static string expectedToken;

//Print parser error
void parseError()
{
    cout << "Parser error on line number " << token.line_Number <<
         ": expected '" << expectedToken << "' but received '" << token.tk_Description << "'\n";
    exit(EXIT_FAILURE);
}

/// Fucntion to create a new node for a non-terminal and assign its possible children to NULL
/// \param nonTermName
/// \return
node_t *createNode(string nonTermName)
{
    node_t *node = new node_t();
    node->child1 = NULL;
    node->child2 = NULL;
    node->child3 = NULL;
    node->child4 = NULL;

    node->label = nonTermName;

    return node;
}

//Main parser function
node_t *parser(){
    EMPTY_TOKEN.tk_Description = "EMPTY";

    //Create node
    node_t *root = NULL;

    //scanner
    scanner(token);

    //Our program always starts with s as the root node.
    root = S();

    //Check for EOF token. If not found throw error
    if(token.ID != EOFtk){
        expectedToken.assign("EOF");
        parseError();
    }
    else{
        cout <<"PARSER CHECK: PASSED\n";
        return root;
    }
    return root;
}

// <S> -> program <V> <B>
node_t *S(){
    //create node
    node_t *node = createNode("<S>");

    //check for keyword program
/*    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/
    if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "PROGRAMtk")){
        //get token from scanner
        scanner(token);

        //assign tokens possible for program
        node->child1 = V();
        node->child2 = B();

        //return node
        return node;
    }
    //pass error
    else{
        expectedToken.assign("program");
        parseError();
    }
    return node;
}

//<B> -> begin <V> <Q> end production
node_t *B(){
    //creat node
    node_t *node = createNode("<B>");

    //For debug
/*    cout<<"***** NOW IN NODE B\n";
    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/

    //Check for begin keyword
    if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "BEGINtk")){
        //get token from scanner
        scanner(token);

        //assign children
        node->child1 = V();
        node->child2 = Q();

        //Check for end keyword
        if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "ENDtk")){
            //scanner
            scanner(token);
            return node;
        }
        //Error for no end token
        else{
            expectedToken.assign("end");
            parseError();
        }
    }
    //no begin token
    else{
        expectedToken.assign("begin");
        parseError();
    }
    return node;
}

//<V> -> empty | var identifier . <V>
node_t *V(){
    //create node
    node_t *node = createNode("<V>");

    //For debug
    /*cout<<"***** NOW IN NODE V\n";
    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/

    //check if var keyword is present
    if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "VARtk")){
        //scanner
        scanner(token);

        if(token.ID == IDtk){
            node->tokens.push_back(token);
            //scanner
            scanner(token);

            if((token.ID == OPtk) && (op_map[token.tk_Description] == "PERIODtk")){
                //push back period
                node->tokens.push_back(token);
                //scanner
                scanner(token);
                node->child1 = V();
            }
            else{
                expectedToken.assign(".");
                parseError();
            }
        }
        else{
            expectedToken.assign("identifer");
            parseError();
        }
    }
    else{
        node->tokens.push_back(EMPTY_TOKEN);
        return node;
    }
    return node;
}

//<M> -> <H> + <M> | <H> - <M> | <H> / <M> | <H> * <M> | <H>
//God this one sucks
node_t *M(){
    //create node
    node_t *node = createNode("<M>");

    //For debug
   /* cout<<"***** NOW IN NODE M\n";
    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/

    //call <H>
    node->child1 = H();

    //test for op
    if(token.ID == OPtk){
        //check if op is +
        if(op_map[token.tk_Description] == "PLUStk"){
            //store + in M node
            node->tokens.push_back(token);
            scanner(token);
            //set child 2 to M
            node->child2 = M();
        }
        else if(op_map[token.tk_Description] == "MINUStk"){
            //store - in M node
            node->tokens.push_back(token);
            scanner(token);
            //set child 2 to M
            node->child2 = M();
        }
        else if(op_map[token.tk_Description] == "SLASHtk"){
            //store / in M node
            node->tokens.push_back(token);
            scanner(token);
            //set child 2 to M
            node->child2 = M();
        }
        else if(op_map[token.tk_Description] == "ASTERIKtk"){
            //store * in M node
            node->tokens.push_back(token);
            scanner(token);
            //set child 2 to M
            node->child2 = M();
        }
    }

    return node;
}

//<H> -> & <R> | <R>
node_t *H(){
    //create node
    node_t *node = createNode("<H>");

/*    //For debug
    cout<<"***** NOW IN NODE H\n";
    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/

    //test for &
    if((token.ID == OPtk) && (op_map[token.tk_Description] == "ANDtk")){
        //scanner
        scanner(token);
        //set child
        node->child1 = R();
        return node;
    }
    else{
        node->child1 = R();
        return node;
    }
}

//<R> -> identifier | number
node_t *R()
{
    //create the node
    node_t *node = createNode("<R>");

/*    //For debug
    cout<<"***** NOW IN NODE R\n";
    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/

    //test for idenifer
    if((token.ID == IDtk) || (token.ID == INTtk)){
        //push back id
        node->tokens.push_back(token);
        //scanner
        scanner(token);
        return  node;
    }
    else{
        expectedToken.assign("identifer | number");
        parseError();
    }
    return node;
}

//<Q> -> <T> # <Q> | empty
node_t *Q(){
    //create node
    node_t *node = createNode("<Q>");

/*    //For debug
    cout<<"***** NOW IN NODE Q\n";
    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/

    //Test for keywords scan, write, begin, if, repeat, let
    //Becuase of grammar <T> -> <A> , | <W> , | <B> | <I> , | <G> , | <E> ,

    if((token.ID == KEYWORDtk ) && ((keyword_map[token.tk_Description] == "SCANtk") || (keyword_map[token.tk_Description] == "WRITEtk") ||
                                   (keyword_map[token.tk_Description] == "BEGINtk")|| (keyword_map[token.tk_Description] == "IFtk") ||
                                   (keyword_map[token.tk_Description] == "REPEATtk") || (keyword_map[token.tk_Description] == "LETtk"))){
        //scanner
        //scanner(token);

        //set child to T()
        node->child1 = T();

        if((token.ID == OPtk) && (op_map[token.tk_Description]=="HASHtk")){
            //pushback token
            node->tokens.push_back(token);
            //scanner
            scanner(token);
            node->child2 = Q();
        }
        else{
            expectedToken.assign("#");
            parseError();
        }
        return node;
    }
    //Empty production
    else{
        node->tokens.push_back(EMPTY_TOKEN);
        return node;
    }
}

//<T> -> <A> , | <W> , | <B> | <I> , | <G> , | <E> ,

node_t *T(){
    //create node
    node_t *node = createNode("<T>");

/*    //For debug
    cout<<"***** NOW IN NODE T\n";
    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/


    //Test for <A>
    if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "SCANtk")){

        //Child is A()
        node->child1 = A();
        //scanner
        //scanner(token);
        if((token.ID == OPtk) && (op_map[token.tk_Description] == "COMMAtk")){
            //push back comma
            node->tokens.push_back(token);
            //scanner
            scanner(token);
            return node;
        }
        else{
            expectedToken.assign(",");
            parseError();
        }

    }
    else if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "WRITEtk")){
        //scanner
        //scanner(token);
        //Child is W()
        node->child1 = W();
        if((token.ID == OPtk) && (op_map[token.tk_Description] == "COMMAtk")){
            //push back comma
            node->tokens.push_back(token);
            //scanner
            scanner(token);
            return node;
        }
        else{
            expectedToken.assign(",");
            parseError();
        }
    }
    else if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "BEGINtk")){
        //scanner
        //scanner(token);
        //Child is B()
        node->child1 = B();

    }
    else if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "IFtk")){
        //scanner
        //scanner(token);
        //Child is I()
        node->child1 = I();
        if((token.ID == OPtk) && (op_map[token.tk_Description] == "COMMAtk")){
            //push back comma
            node->tokens.push_back(token);
            //scanner
            scanner(token);
            return node;
        }
        else{
            expectedToken.assign(",");
            parseError();
        }
    }
    else if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "REPEATtk")) {
        //scanner
        //scanner(token);
        //Child is G()
        node->child1 = G();
        if ((token.ID == OPtk) && (op_map[token.tk_Description] == "COMMAtk")) {
            //push back comma
            node->tokens.push_back(token);
            //scanner
            scanner(token);
            return node;
        } else {
            expectedToken.assign(",");
            parseError();
        }
    }
    else if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "LETtk")){
        //scanner
        //scanner(token);
        //Child is E()
        node->child1 = E();
        if((token.ID == OPtk) && (op_map[token.tk_Description] == "COMMAtk")){
            //push back comma
            node->tokens.push_back(token);
            //scanner
            scanner(token);
            return node;
        }
        else{
            expectedToken.assign(",");
            parseError();
        }
    }
    //None of the tokens expected were found
    else{
        expectedToken.assign("<A> , | <W> , | <B> | <I> , | <G> , | <E> ,");
        parseError();
    }
    return node;
}

//<A> -> scan identifier | scan number
node_t *A(){
    //create the node
    node_t *node = createNode("<A>");

/*    //For debug
    cout<<"***** NOW IN NODE A\n";
    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/


    //test for scan
    if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "SCANtk")){
        //scanner
        scanner(token);
        //check for id
        if(token.ID == IDtk){
            //push value in identifer to <A> vecotr
            cout<<"DEBUG: IDtk encountered. Pusshing back value to stack\n";
            node->tokens.push_back(token);
            //scanner
            scanner(token);
            return node;
        }
        else if(token.ID == INTtk){
            //push the interger value to <A> vecotr
            cout<<"DEBUG: INTtk encountered. Pusshing back value to stack\n";
            node->tokens.push_back(token);
            //scanner
            scanner(token);
            return node;
        }
        else{
            expectedToken.assign("identifier or number");
            parseError();
        }
    }
    else{
        expectedToken.assign("scan");
        parseError();
    }
    return node;
}

//<W> -> write <M>
node_t *W(){
    //create the node
    node_t *node = createNode("<W>");

/*    //For debug
    cout<<"***** NOW IN NODE W\n";
    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/

    //test for write keyword
    if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "WRITEtk")){
        //scanner
        scanner(token);
        //child id M
        node->child1 = M();
        return node;
    }
    else{
        expectedToken.assign("write");
        parseError();
    }
    return node;
}

//<I> -> if [ <M> <Z> <M> ] <T>
node_t *I(){
    //create the node
    node_t *node = createNode("<I>");

/*    //For debug
    cout<<"***** NOW IN NODE I\n";
    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/


    //Test for if
    if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "IFtk")){
        //scanner
        scanner(token);
        //check for [
        if((token.ID == OPtk) && (op_map[token.tk_Description] == "LEFTBRACKETtk")){
            //push back [
            //node->tokens.push_back(token);
            scanner(token);
            //set the children
            node->child1 = M();
            node->child2 = Z();
            node->child3 = M();

            //check for ]
            if((token.ID == OPtk) && (op_map[token.tk_Description] == "RIGHTBRACKETtk")){
                //push back ]
                //node->tokens.push_back(token);
                //scanner
                scanner(token);
                node->child4 = T();
                return node;
            }
            else{
                expectedToken.assign("]");
                parseError();
            }
        }
        else{
            expectedToken.assign("[");
            parseError();
        }
    }
    else{
        expectedToken.assign("if");
        parseError();
    }
    return node;
}

//<G> -> repeat [ <M> <Z> <M> ] <T>
node_t *G(){
    //create the node
    node_t *node = createNode("<G>");

/*    //For debug
    cout<<"***** NOW IN NODE G\n";
    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/


    //Test for repeat
    if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "REPEATtk")){
        //scanner
        scanner(token);
        //check for [
        if((token.ID == OPtk) && (op_map[token.tk_Description] == "LEFTBRACKETtk")){
            scanner(token);
            //set the children
            node->child1 = M();
            node->child2 = Z();
            node->child3 = M();

            //check for ]
            if((token.ID == OPtk) && (op_map[token.tk_Description] == "RIGHTBRACKETtk")){
                //scanner
                scanner(token);
                node->child4 = T();
                return node;
            }
            else{
                expectedToken.assign("]");
                parseError();
            }
        }
        else{
            expectedToken.assign("[");
            parseError();
        }
    }
    else{
        expectedToken.assign("repeat");
        parseError();
    }
    return node;
}

//<E> -> let identifier : <M>
node_t *E(){
    //create the node
    node_t *node = createNode("<E>");

/*    //For debug
    cout<<"***** NOW IN NODE E\n";
    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/

    //test for let
    if((token.ID == KEYWORDtk) && (keyword_map[token.tk_Description] == "LETtk")){
        //scanner
        scanner(token);
        //check for identifier
        if(token.ID == IDtk){
            //store the id in vector
            node->tokens.push_back(token);
            //scanner
            scanner(token);
            //check for :
            if((token.ID == OPtk) && (op_map[token.tk_Description] == "COLONtk")){
                //scanner
                scanner(token);
                //set child
                node-> child1 = M();
                return node;
            }
            else{
                expectedToken.assign(":");
                parseError();
            }
        }
        else{
            expectedToken.assign("identifier");
            parseError();
        }
    }
    else{
        expectedToken.assign("let");
        parseError();
    }
    return node;
}

//FINALLY THE LAST ONE DEAR GOD
//<Z> -> < | > | : | = | = =
node_t *Z(){
    //create the node
    node_t *node = createNode("<Z>");

/*    //For debug
    cout<<"***** NOW IN NODE Z\n";
    cout<<"DEBUG: current token: " + token.tk_Description + "\n";*/

    // Test for <
    if((token.ID == OPtk) && (op_map[token.tk_Description] == "LESSTHANtk")){
        //insert < into vector
        node->tokens.push_back(token);
        //scanner
        scanner(token);
        return node;
    }
    else if((token.ID == OPtk) && (op_map[token.tk_Description] == "GREATERTHANtk")){
        //insert > into vector
        node->tokens.push_back(token);
        //scanner
        scanner(token);
        return node;
    }
    else if((token.ID == OPtk) && (op_map[token.tk_Description] == "COLONtk")){
        //inssert : into vector
        node->tokens.push_back(token);
        //scanner
        scanner(token);
        return node;
    }
    else if((token.ID == OPtk) && (op_map[token.tk_Description] == "EQUALStk")){
        //insert = into vector
        node->tokens.push_back(token);
        //scanner
        scanner(token);
        //test for = in the case of ==
        if((token.ID == OPtk) && (op_map[token.tk_Description] == "EQUALStk")){
            //insert = into vector
            node->tokens.push_back(token);
            //scanner
            scanner(token);
            return node;
        }
        //error if another token besides = follows the first =
        else if((token.ID == OPtk) && (op_map[token.tk_Description] != "EQUALStk")){
            expectedToken.assign("==");
            parseError();
        }
        //return for single = case
        else{
            return node;
        }
    }
    else{
        expectedToken.assign("< | > | : | = | ==");
        parseError();
    }
    return node;
}